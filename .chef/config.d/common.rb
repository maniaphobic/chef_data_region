require 'chef/data_region'

# Map data bag names to their expansion patterns
Chef::DataRegion.bags = {
  'secure' => 'secure-%<region>s'
}

# Define the path to the region node attribute
Chef::DataRegion.region = %w[local region]
