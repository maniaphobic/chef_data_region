cwd = Dir.getwd
require_relative File.join(cwd, '.chef', 'config.d', 'common')
encrypted_data_bag_secret File.join(cwd, '.chef', 'secrets', 'dev.key')
