cwd = Dir.getwd
#
chef_repo_path cwd
chef_server_url 'http://localhost:8889/'
client_key File.join(cwd, '.chef', 'default.pem')
node_name 'default'
