# Chef Data Region Demonstration Repository

This Chef repository demonstrates the [data region
gem](https://bitbucket.org/maniaphobic/ruby_gems_chef_data_region).

## Usage

Run `rake` to invoke chef-client for each of the demonstration
regions. The `service::default` recipe calls the overloaded
`data_bag_item` method, which decrypts the data that varies by region
name.
